Ext.define('senchaApp.controller.Multimedia', {
    extend: 'Ext.app.Controller',
    config: {
        views: ['MultimediaTab', 'multimedia.Menu', 'multimedia.Audio', 'multimedia.Video'],
        refs: {
            container: '#multimediaContainer',
            audioBtn: '#audioBtn',
            videoBtn: '#videoBtn'
        },
        control: {
            'audioBtn': {
                tap: 'goToAudioView'
            },
            'videoBtn': {
                tap: 'goToVideoView'
            }
        }
    },

    goToAudioView: function() {
        this.getContainer().push({
            xtype: 'multimediaAudio',
            title: 'Audio'
            });
    },
    goToVideoView: function() {
        this.getContainer().push({
            xtype: 'multimediaVideo',
            title: 'Video'
            });
    }
});