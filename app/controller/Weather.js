Ext.define('senchaApp.controller.Weather', {
    extend: 'Ext.app.Controller',

    config: {
        views: ['WeatherTab', 'weather.Menu', 'weather.Actual'],// 'weather.Forecast'],
        refs: {
            container: '#weatherContainer',
            cityTxt: '#cityTxt',
            cityBtn: '#cityBtn',
            forecastBtn: '#forecastBtn'
        },
        control: {
            'cityBtn': {
                tap: 'goToActualWeather'
            },
            'forecastBtn': {
                tap: 'goToForecastWeather'
            }
        },
        stores: ['ActualWeather', 'ForecastWeather']
    },

    goToActualWeather: function () {
        var city = this.getCityTxt().getValue();
        var data;
        if (city != null && city.length > 0) {
            Ext.Viewport.setMasked({
                xtype: 'loadmask',
                message: 'Pobieram dane...'
            });
            var store = Ext.getStore('ActualWeatherId');
            store.load({
                params: {
                    q: city
                },
                callback: function (records, opertion, success) {
                    Ext.Viewport.setMasked(false);
                    if (success == true) {
                        data = {
                            city: records[0].get('name'),
                            country: records[0].get('sys').country,
                            temp: Math.round(parseFloat(records[0].get('main').temp) - 273.15),
                            img: 'http://openweathermap.org/img/w/' + records[0].get('weather')[0].icon + '.png',
                            background: records[0].get('weather')[0].main.toLowerCase(),
                            pressure: Math.round(parseFloat(records[0].get('main').pressure)),
                            humidity: records[0].get('main').humidity
                        };
                        console.log(data);
                        Ext.getCmp('weatherContainer')
                            .push({
                                xtype: 'weatherActual',
                                //title: 'Pogoda - ' + records[0].get('name'),
                                data: data
                        });
                    }
                    else {
                        Ext.Msg.alert('Błąd', 'Wystąpił nieoczekiwany błąd podczas pobierania danych...');
                    }
                },
                scope: this
            });
        }
        else {
            Ext.Msg.alert('Błąd', 'Proszę podać nazwę miejscowości...');
        }
    },

    goToForecastWeather: function () {
        //var city = this.getCityTxt().getValue();
        //var data;
        //if (city != null && city.length > 0) {
        //    Ext.Viewport.setMasked({
        //        xtype: 'loadmask',
        //        message: 'Pobieram dane...'
        //    });
            var store = Ext.getStore('ForecastWeatherId');
            //var store = Ext.create('senchaApp.store.ForecastWeather');
            store.load({
                params: {
                    q: 'Wieluń'
                },
                scope: this,
                callback: function (records, opertion, success) {
                    console.log('!!!');
                    //Ext.Viewport.setMasked(false);
                    if (success) {
                    //    data = {
                    //        city: records[0].get('name'),
                    //        country: records[0].get('sys').country,
                    //        temp: Math.round(parseFloat(records[0].get('main').temp) - 273.15),
                    //        img: 'http://openweathermap.org/img/w/' + records[0].get('weather')[0].icon + '.png',
                    //        background: records[0].get('weather')[0].main.toLowerCase(),
                    //        pressure: Math.round(parseFloat(records[0].get('main').pressure)),
                    //        humidity: records[0].get('main').humidity
                    //    };
                        console.log(records[0]);
                        //Ext.getCmp('weatherContainer')
                        //    .push({
                        //        xtype: 'weatherActual',
                        //        //title: 'Pogoda - ' + records[0].get('name'),
                        //        data: data
                        //});
                    }
                    else {
                        Ext.Msg.alert('Błąd', 'Wystąpił nieoczekiwany błąd podczas pobierania danych...');
                    }
                }
            });
            console.log(store.getCount());
        //}
        //else {
        //    Ext.Msg.alert('Błąd', 'Proszę podać nazwę miejscowości...');
        //}
    }
});

    //    Ext.data.JsonP.request({
    //        url: 'http://api.openweathermap.org/data/2.5/weather?',
    //        callbackKey: 'callback',
    //        params: {
    //            appid: 'd9c18ced2f48846634dcde751ec21134',
    //            q: city
    //        },
    //        success: function (response) {
    //            console.log(response.name);
    //            data = {
    //                city: response.name,
    //                country: response.sys.country,
    //                temp: Math.round(parseFloat(response.main.temp) - 273.15),
    //                img: 'http://openweathermap.org/img/w/' + response.weather[0].icon + '.png',
    //                background: response.weather[0].main.toLowerCase(),
    //                pressure: Math.round(parseFloat(response.main.pressure)),
    //                humidity: response.main.humidity
    //            };
    //            console.log(data);
    //            //me.goToActualWeather(data);
    //        },
    //        failure: function (response) {
    //            console.log(response);
    //            Ext.Msg.alert('Błąd', 'Wystąpił nieoczekiwany błąd podczas pobierania danych...');
    //        }
    //    });
    //}