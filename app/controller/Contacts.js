Ext.define('senchaApp.controller.Contacts', {
    extend: 'Ext.app.Controller',

    config: {
        views: ['ContactsTab', 'contacts.List', 'contacts.Add', 'contacts.Form'],
        stores: ['Contacts'],
        refs: {
            container: '#contactsContainer',
            contactsList: "#contactsList",
            contactsForm: '#contactsForm',
            addBtn: '#addBtn',
            editBtn: '#editBtn',
            removeBtn: '#removeBtn',
            saveBtn: '#saveBtn'
        },
        control: {
            'contactsList': {
                itemtap: 'goToDetailContact'
            },
            'addBtn': {
                tap: 'goToAddContact'
            },
            'editBtn': {
                tap: 'goToEditContact'
            },
            'removeBtn': {
                tap: 'removeContact'
            },
            'saveBtn': {
                tap: 'saveContact'
            }
        }
    },

    goToDetailContact: function (list, index, element, record) {
        //console.log(record.data.name);
        this.contact = record.data;
        this.getContainer().push({
            xtype: 'contactDetail',
            title: record.data.name + ' ' + record.data.surname,
            data: record.data
        });
    },

    goToAddContact: function () {
        this.getContainer().push({
            xtype: 'contactsForm',
            title: 'Nowy Kontakt'
        });
    },

    goToEditContact: function () {
        this.getContainer().push({
            xtype: 'contactsForm',
            title: 'Edycja Kontaktu'
        });
        this.getContactsForm().setValues(this.contact);
    },

    removeContact: function () {
        Ext.Msg.confirm("Uwaga", "Czy na pewno chcesz usunąć kontakt " + this.contact.name + " " + this.contact.surname + "?", function (button) {
            if (button == 'yes') {
                Ext.getStore('ContactsId').each(function (record) {
                    if (record.get('id') == this.contact.id) {
                        Ext.getStore('ContactsId').remove(record);
                    }
                }, this);
                this.getContainer().reset();//.setActiveItem(0);
            } else {
                return false;
            }
        }, this);

        //Ext.Msg.show({
        //    title: "Uwaga",
        //    message: "Czy na pewno chcesz usunąć kontakt " + this.contact.name + " " + this.contact.surname + "?",
        //    buttons: [{
        //        itemId: 'yes',
        //        text: 'Usuń'
        //    }, {
        //        itemId: 'no',
        //        text: 'Anuluj'
        //    }],
        //    function (button) {
        //        if (button == 'yes') {
        //            Ext.getStore('ContactsId').each(function (record) {
        //                if (record.get('id') == this.contact.id) {
        //                    Ext.getStore('ContactsId').remove(record);
        //                }
        //            }, this);
        //            //Ext.getCmp('contactsContainer').reset();
        //            Ext.getApplication().getController('Contacts').getContainer().reset();
        //        }
        //        else {
        //            return false;
        //        }
        //    },
        //    scope: this
        //});
    },

    saveContact: function (button, e, options) {
        var formObj = button.up('Form');
        var formData = formObj.getValues();
        var store = Ext.getStore('ContactsId');

        var contact = Ext.create('senchaApp.model.Contact', {
            name: formData.name,
            surname: formData.surname,
            phone: formData.phone,
            email: formData.email
        });

        var errs = contact.validate();
        var msg = 'Wystąpiły następujące błędy:<br/>';

        if (!errs.isValid()) {
            errs.each(function (err) {
                msg += '- ' + err.getMessage() + '<br/>';
            });

            Ext.Msg.alert('Uwaga!', msg);

        }
        else {
            if (formData.id == null) {
                var id = 0;
                store.each(function (record) {
                    if (record.get('id') > id) {
                        id = record.get('id');
                    }
                }, this);
                id++;
                contact.set('id', id);
                if (formData.face == '') {
                    contact.set('face', 'http://www.bartlomiej-zmudzinski.pl/magisterka/whois.png');
                }
                console.log(contact);
                store.add(contact);
                store.sync();
                this.getContainer().reset();
            }
            else {
                store.each(function (record) {
                    if (record.get('id') == formData.id) {
                        record.set('name', formData.name);
                        record.set('surname', formData.surname);
                        record.set('phone', formData.phone);
                        record.set('email', formData.email);
                    }
                }, this);
                store.sync();
                this.getContainer().reset();
            }
        }
    }
});