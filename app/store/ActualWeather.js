Ext.define('senchaApp.store.ActualWeather', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.JsonP'
    ],

    config: {
        model: 'senchaApp.model.ActualWeather',
        storeId: 'ActualWeatherId',
        proxy: {
            type: 'jsonp',
            url: 'http://api.openweathermap.org/data/2.5/weather?',
            callbackKey: 'callback',
            extraParams: {
                appid: 'd9c18ced2f48846634dcde751ec21134'
                //q: 'Wieluń'
            },
            reader: {
                type: 'json'
            }
        }
    }
});