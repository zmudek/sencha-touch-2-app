Ext.define('senchaApp.store.Contacts', {
    extend: 'Ext.data.Store',

    config: {
        model: 'senchaApp.model.Contact',
        storeId: 'ContactsId',
        autoLoad: true,
        data: [{
			id: 0,
			name: 'Piotr',
			surname: 'Nowak',
			phone: 123456789,
			email: 'abc@cba.pl',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/1.jpg'
		}, {
			id: 1,
			name: 'Jan',
			surname: 'Kowalski',
			phone: 111222333,
			email: '',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/2.jpg'
		}, {
			id: 2,
			name: 'Adam',
			surname: 'Parzybut',
			phone: 222444666,
			email: '',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/3.jpg'
		}, {
			id: 3,
			name: 'Stefan',
			surname: 'Sosnowski',
			phone: 999888777,
			email: 'a@a.aa',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/whois.png'
		}, {
			id: 4,
			name: 'Krzysztof',
			surname: 'Tokarek',
			phone: 987654321,
			email: 'xyz@wp.pl',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/4.jpg'
		}, {
			id: 5,
			name: 'Mateusz',
			surname: 'Perka',
			phone: 555000555,
			email: 'zzz@gmail.com',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/5.jpg'
		}],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                rootProperty: 'contacts'
            }
        }
    }
});