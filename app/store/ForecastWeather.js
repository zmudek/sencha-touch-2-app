Ext.define('senchaApp.store.ForecastWeather', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.Ajax',
        'Ext.data.reader.Xml'
    ],

    config: {
        model: 'senchaApp.model.ForecastWeather',
        storeId: 'ForecastWeatherId',
        proxy: {
            type: 'ajax',
            url: 'http://api.openweathermap.org/data/2.5/forecast/daily?',
            callbackKey: 'callback',
            mathod:'GET',
            cors: true,
            useDefaultXhrHeader: false,
            extraParams: {
                appid: 'd9c18ced2f48846634dcde751ec21134',
                mode: 'xml',
                units: 'metric',
                cnt: 16
                //q: 'Wieluń'
            },
            reader: {
                type: 'xml',
                rootProperty: 'weatherdata'
            }
        }
    }
});