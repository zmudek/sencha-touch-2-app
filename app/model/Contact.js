Ext.define('senchaApp.model.Contact', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: 'id',
        fields: [
            { name: 'id', type: 'int' },
            { name: 'name' },
            { name: 'surname' },
            { name: 'phone' },
            { name: 'email' },
            { name: 'face' }
        ],
        validations: [
            {
                field: 'name',
                type: 'presence',
                message: 'Imię jest obowiązkowe.'
            },
            {
                field: 'surname',
                type: 'presence',
                message: 'Nazwisko jest obowiązkowe.'
            },
            {
                field: 'phone',
                type: 'presence',
                message: 'Numer telefonu jest obowiązkowy.'
            },
            {
                field: 'phone',
                type: 'format',
                matcher: /^\d{9}$/,
                message: 'Numer telefonu musi się składać z 9 znaków.'
            },
            {
                field: 'email',
                type: 'email',
                message: 'Adres e-mail jest niepoprawny.'
            }
        ]
    }
});