Ext.define('senchaApp.model.ActualWeather', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['name', 'main', 'country', 'sys', 'weather']
    }
});