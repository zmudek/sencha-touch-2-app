Ext.define('senchaApp.model.DailyForecastWeather', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'day',
                mapping: '@day'
            },
            {
                name: 'icon',
                mapping: 'symbol@var'
            },
            {
                name: 'temp',
                mapping: 'temperature@day'
            },
            {
                name: 'pressure',
                mapping: 'pressure@value'
            },
            {
                name: 'humidity',
                mapping: 'humidity@value'
            }
        ]

        //associations: [
        //    {
        //        associationsKey: 'forecast',
        //        type: 'hasMany',
        //        model: 'senchaApp.model.DailyForecastWeather',
        //        reader: {
        //            type: 'xml',
        //            record: 'time',
        //            root: 'forecast'
        //        }
        //    }
        //]
    }
});