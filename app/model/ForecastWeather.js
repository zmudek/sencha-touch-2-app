Ext.define('senchaApp.model.ForecastWeather', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'city',
                mapping: 'location.name'
            },
            {
                name: 'country',
                mapping: 'location.country'
            }
        ],

        associations: [
            {
                associationsKey: 'forecast',
                type: 'hasMany',
                model: 'senchaApp.model.DailyForecastWeather',
                reader: {
                    type: 'xml',
                    record: 'time',
                    rootProperty: 'forecast'
                }
            }
        ]
    }
});