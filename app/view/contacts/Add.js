Ext.define("senchaApp.view.contacts.Add", {
    extend : 'Ext.Container',
    xtype: "contactsAdd",

    config: {
        title: 'Nowy Kontakty',

        items: [
            {
                xtype: 'contactsForm'
            }
        ]
    },

    show: function () {
        Ext.get('addBtn').hide();
        Ext.get('editBtn').hide();
        Ext.get('removeBtn').hide();
        this.callParent(arguments);
    }
});