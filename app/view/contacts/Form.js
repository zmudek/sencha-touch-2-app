Ext.define("senchaApp.view.contacts.Form", {
    extend: 'Ext.form.Panel',
    alias: 'widget.Form',
    xtype: 'contactsForm',
    requires: [
        'Ext.form.FieldSet', 'Ext.field.Email', 'Ext.field.Number'
    ],

    config: {
        itemId: 'contactsForm',
        items: [
            {
                xtype: 'fieldset',
                title: 'Nowy Kontakt',
                items: [
                    {
                        xtype: 'textfield',
                        label: 'Imię',
                        labelWrap: true,
                        name: 'name',
                        //placeHolder: 'Enter Username'
                        required: true
                    },
                    {
                        xtype: 'textfield',
                        label: 'Nazwisko',
                        labelWrap: true,
                        name: 'surname',
                        //placeHolder: 'Enter Password'
                        required: true
                    },
                    {
                        xtype: 'numberfield',
                        label: 'Nr tel.',
                        labelWrap: true,
                        name: 'phone',
                        //placeHolder: 'email@example.com'
                        required: true
                    },
                    {
                        xtype: 'emailfield',
                        label: 'E-mail',
                        labelWrap: true,
                        name: 'email'
                        //placeHolder: 'email@example.com'
                    },
                    {
                        xtype: 'numberfield',
                        name: 'id',
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        name: 'face',
                        hidden: true
                    }
                ]
            },
            {
                xtype: 'button',
                id: 'saveBtn',
                width: '100%',
                text: 'Zapisz'
            }
        ]
        //listeners: [{
        //    fn: 'onFormSave',
        //    event: 'tap',
        //    delegate: '#save'
        //}]
    },

    show: function () {
        Ext.get('addBtn').hide();
        Ext.get('editBtn').hide();
        Ext.get('removeBtn').hide();
        this.callParent(arguments);
    }
});