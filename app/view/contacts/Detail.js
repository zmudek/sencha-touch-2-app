Ext.define("senchaApp.view.contacts.Detail", {
    //extend: "Ext.NavigationView",
    extend : 'Ext.Container',
    xtype: "contactDetail",

    config: {
        tpl: [
            '<div class="container" style="border: 1px solid #000;">',
            '<img class="img-circle" src="{ face }">',
            '<table style="border-collapse: collapse;">',
            '<tr style="border: 1px solid #cccccc;"><td><h2 style="margin: 0;">{ name } { surname }</h2><p style="color: #666">Imię i nazwisko</p></td></tr>',
            '<tr style="border: 1px solid #cccccc;"><td><h2 style="margin: 0;">{ phone }</h2><p style="color: #666">Numer telefonu</p></td></tr>',
            '<tpl if="email != \'\'">',
            '<tr style="border: 1px solid #cccccc;"><td><h2 style="margin: 0;">{ email }</h2><p style="color: #666">E-mail</p></td></tr>',
            '</tpl>',
            '</table>',
            '</div>'
        ]
    },

    show: function () {
        Ext.get('addBtn').hide();
        Ext.get('editBtn').show();
        Ext.get('removeBtn').show();
        this.callParent(arguments);
    }
});