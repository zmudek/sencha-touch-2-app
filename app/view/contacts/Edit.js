Ext.define("senchaApp.view.contacts.List", {
    //extend: "Ext.NavigationView",
    extend : 'Ext.dataview.List',
    xtype: "contactsList",

    config: {
        title: 'Kontakty',

        items: [
            {
                html: '<h4 style="text-align: center;">Co chcesz odtworzyć?</h4>'
            }
        ]
    },

    show: function () {
        Ext.get('addBtn').show();//.hide();//.setHidden(false);
        this.callParent(arguments);
    }
});