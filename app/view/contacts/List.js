Ext.define("senchaApp.view.contacts.List", {
    extend : 'Ext.List',
    xtype: "contactsList",
    id: "contactsList",

    requires: [
        'senchaApp.store.Contacts'
    ],

    config: {
        title: 'Kontakty',
        store: 'ContactsId',
        scrollable: true,
        //fullscreen: true,

        itemTpl: '<img class="avatar" src="{ face }" /><h3>{ name } { surname }</h3>'
    },

    show: function () {
        Ext.get('addBtn').show();
        Ext.get('editBtn').hide();
        Ext.get('removeBtn').hide();
        this.callParent(arguments);
    }
});