Ext.define("senchaApp.view.weather.Actual", {
    extend: "Ext.Container",
    xtype: "weatherActual",

    config: {
        title: 'Aktualna Pogoda',
        tpl: [
            '<h2>{ city }, { country }</h2>',
            '<p><img src="{ img }"></p>',
            '<table style="border: 0;">',
            '<tr><td>Temperatura:</td><td>{ temp }°</td></tr>',
            '<tr><td>Ciśnienie:</td><td>{ pressure }hPa</td></tr>',
            '<tr><td>Wilgotność:</td><td>{ humidity }%</td></tr>',
            '</table>'
        ],
        margin: 20,
        items: [
            {
                xtype: 'button',
                docked: 'bottom',
                ui: 'confirm',
                text: 'Prognoza na najbliższe 16 dni',
                id: 'forecastBtn'
            }
        ]
    }
});