Ext.define("senchaApp.view.weather.Menu", {
    extend: "Ext.Container",
    xtype: "weatherMenu",

    config: {
        title: 'Pogoda',
        defaults: {
            margin: 20
        },

        items: [
            {
                html: '<h4 style="text-align: center;">Gdzie sprawdzić pogodę?</h4>'
            },
            {
                name: 'city',
                xtype: 'textfield',
                id: 'cityTxt'
            },
            {
                xtype: 'button',
                ui: 'confirm',
                text: 'Sprawdź',
                id: 'cityBtn'

                //listeners: {
                //    scope: this,
                //    tap: function () {
                //        console.log(Ext.getCmp('cityField').getValue());
                //        Ext.getCmp('weatherContainer').push(Ext.create('senchaApp.view.WeatherActual'));
                //
                //        //setActiveItem(Ext.create('senchaApp.view.WeatherActual'));
                //        //Ext.getCmp('weatherTabPanel').setActiveItem(1);
                //        //Ext.getCmp('container').removeAll();// destroy();
                //        //Ext.getCmp('container').setActiveItem(Ext.create('senchaApp.view.Carousel'));
                //        //Ext.getCmp('tabPanel').query("#navigationBar")[0].titleComponent.setTitle('changedTitle');
                //        //Ext.getCmp('back').show();
                //    }
                //}
            }

        ]
    }
});