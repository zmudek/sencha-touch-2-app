Ext.define("senchaApp.view.MultimediaTab", {
    extend: 'Ext.NavigationView',
    xtype: 'multimediaTabPanel',

    config: {
        title: 'Multimedia',
        iconCls: 'action',

        navigationBar: {
            docked: 'top',
            hidden: true
        },

        items: {
            id: 'multimediaContainer',
            xtype: 'navigationview',
            defaultBackButtonText: 'Wróć',
            items: [
            {
                xtype: 'multimediaMenu'
            }]
        }
    }

    //initialize: function () {
    //    this.callParent(arguments);
    //}
});