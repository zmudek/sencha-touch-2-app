Ext.define("senchaApp.view.GalleryTab", {
    extend: 'Ext.Container',
    xtype: 'galleryTabPanel',

    config: {
        title: 'Galeria',
        iconCls: 'bookmarks',

        id: 'galleryContainer',
        cls: 'gallery',
        // scrollable: true,
        // styleHtmlContent: true,
        tpl: [
            '<div class="gallery" id="photos">',
            '<tpl for=".">',
            '<img src="{min}" class="thumbnail" data-fullimage="{src}"/>',
            '</tpl>',
            '</div>'
        ],
        items: {
            docked: 'top',
            xtype: 'titlebar',
            title: 'Galeria'
        }
    },

    initialize: function () {
        var me = this;
        me.element.on('tap', function (e, el) {
            me.showGalleryCarousel(el);
        }, me, {
            delegate: 'img.thumbnail'
        });

        me.loadImages();
        me.callParent(arguments);
    },

    // Przygotowuję zestaw zdjęć wraz z miniaturami
    loadImages: function () {
        var me = this;

        var photos = [];
        for (var i = 1; i <= 20; i++) {
            var n = i.toString();
            if (i < 10) {
                n = '0' + n;
            }
            var photo = {
                src: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '.jpg',
                min: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '_min.jpg'
            };
            photos.push(photo);
        }
        me.photos = photos;
        me.setData(photos);
    },

    //Powiększa obraz i tworzy Gallery Carousel
    showGalleryCarousel: function (clickedImage) {
        var me = this;

        var galleryCarousel = Ext.Viewport.add({
            xtype: 'galleryCarousel',
            images: me.photos
        });

        // Zamyka Carousel po kliknięciu w zdjęcie
        galleryCarousel.element.on('tap', function (e, el) {
            galleryCarousel.hide(true);

            Ext.defer(function () {
                Ext.Viewport.remove(galleryCarousel);
            }, 300);
        }, this);//, {
        //    delegate: 'div[data-action="close_carousel"]'
        //});

        // Pobiera i wylicza nr kliknietego zdjęcia
        var clickedImgIndex = 0;
        while ((clickedImage = clickedImage.previousSibling) != null) {
            clickedImgIndex++;
        }

        // Ustawia Carousel na odpowiednim zdjęciu
        galleryCarousel.setActiveItem(clickedImgIndex);

        galleryCarousel.show();
    }
});