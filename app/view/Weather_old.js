Ext.define("senchaApp.view.Weather", {
    extend: "Ext.Container",
    xtype: "weatherTab",

    config: {
        itemId: 'container',

        title: 'Pogoda',
        iconCls: 'star',

        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                title: 'Pogoda',
                items: [{
                    cls: 'back',
                    hidden: true,
                    ui: 'back',
                    action: 'back',
                    align: 'left',
                    text: 'Wróć'
                }
                ]
            }, {
                html: '<h4 style="text-align: center;">Gdzie sprawdzić pogodę?</h4>'
            },
            {
                xtype: 'textfield',
                id: 'cityField'
            },
            {
                xtype: 'button',
                ui: 'confirm',
                text: 'Confirm',
                listeners: {
                    scope: this,
                    tap: function () {
                        //Ext.Msg.alert('Welcome', 'Welcome to my first awesome app!');
                        //Ext.Viewport.setActiveItem(Ext.create('senchaApp.view.Carousel'));
                        //Ext.getCmp('tabPanel').setActiveItem(1);
                        //Ext.getCmp('container').removeAll();// destroy();
                        //Ext.getCmp('container').setActiveItem(Ext.create('senchaApp.view.Carousel'));
                        Ext.getCmp('tabPanel').query("#navigationBar")[0].titleComponent.setTitle('changedTitle');
                        //Ext.getCmp('back').show();
                    }
                }
            }

        ]
    }
});