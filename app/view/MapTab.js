Ext.define("senchaApp.view.MapTab", {
    extend: 'Ext.Container',
    xtype: 'mapTabPanel',
    requires: ['Ext.Map'],

    config: {
        title: 'Mapa',
        iconCls: 'maps',

        navigationBar: {
            docked: 'top',
            hidden: true
        },

        layout: 'fit',

        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                title: 'Mapa'
            },
            {
                id: 'mapContainer',
                xtype: 'map',
                mapOptions: {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 15
                },
                useCurrentLocation: true,
                autoUpdate: false,
                allowHighAccuracy: true
            }
        ]
    },

    initialize: function () {
        var me = this;
        me.callParent(arguments);
        this.initMap();
    },

    initMap: function () {
        //var pos = new google.maps.LatLng(51.1086726, 17.0601578);
        var mapPanel = this.down('map');
        var gMap = mapPanel.getMap();

        if (navigator.geolocation) {
            console.log("Urządzenie wspiera geolokalizację!");
            navigator.geolocation.getCurrentPosition(function (pos) {
                var pos = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                //setTimeout(
                //    function() {
                //        gMap.setCenter(pos);
                //    }, 1000);
                var myLocation = new google.maps.Marker({
                    map: gMap,
                    animation: google.maps.Animation.BOUNCE,
                    position: pos,
                    title: 'Aktualna pozycja'
                });
            });
        } else {
            Ext.Msg.alert('Błąd', 'Niestety nie udało się zlokalizować Twojej pozycji...');
        }
    }
});