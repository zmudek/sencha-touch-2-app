Ext.define("senchaApp.view.WeatherTab", {
    extend: 'Ext.NavigationView',
    xtype: 'weatherTabPanel',

    config: {
        title: 'Pogoda',
        iconCls: 'star',

        navigationBar: {
            docked: 'top',
            hidden: true
        },

        items: {
            id: 'weatherContainer',
            xtype: 'navigationview',
            defaultBackButtonText: 'Wróć',
            items: [
            {
                xtype: 'weatherMenu'
            }]
        }
    }

    //initialize: function () {
    //    this.callParent(arguments);
    //}
});