Ext.define("senchaApp.view.Carousel", {
    extend: 'Ext.carousel.Carousel',
    xtype: 'galleryCarousel',

    config: {
        layout: 'card',
        centered: true,
        modal: true,
        height: '100%',
        width: '100%',
        images: [],
        html: '<div class="close-gallery" data-action="close_carousel"></div>',
        cls: 'gallery-carousel',
        showAnimation: 'popIn',
        hideAnimation: 'popOut',
        indicator: false,
        listeners: {
            // Nasłuchuje kiedy zmieniono zdjęcie
            activeitemchange: function (carousel, newPanel) {
                this.changeImageCount(newPanel);
            }
        }
    },

    initialize: function () {
        var me = this;

        // Dodaje wszystkie zdjęcia do Carousel
        for (var i = 0; i < me.getImages().length; i++) {
            me.add({
                xtype: 'container',
                html: '<img class="gallery-item" src="' + me.getImages()[i].src + '" />',
                index: i + 1
            });
        }

        // Dolny licznik zdjęć
        me.bottomBar = Ext.create('Ext.TitleBar', {
            xtype: 'titlebar',
            baseCls: Ext.baseCSSPrefix + 'infobar',
            name: 'info_bar',
            title: '',
            docked: 'bottom',
            cls: 'gallery-bottombar',
            height: 32
        });

        me.add(me.bottomBar);
        me.callParent(arguments);
    },

    // Odpowiada za wyświetlanie numeru zdjęcia
    changeImageCount: function (activePanel) {
        var me = this;
        me.bottomBar.setTitle(activePanel.config.index + ' z ' + me.getImages().length);
    }
});