Ext.define('senchaApp.view.Main', {
    extend: 'Ext.TabPanel',
    xtype: 'main',

    config: {
        fullscreen: true,
        tabBarPosition: 'bottom',
        defaults: {
            styleHtmlContent: true,
            scrollable: true,
            margin: 0,
            padding: 0
        },
        id: 'tabPanel',
		
		items: [
			{ xtype: 'contactsTabPanel' },
			{ xtype: 'weatherTabPanel' },
			{ xtype: 'galleryTabPanel' },
			{ xtype: 'multimediaTabPanel' },
			{ xtype: 'mapTabPanel' },
		]
    },

    initialize: function() {
        this.callParent(arguments);
        // this.add(Ext.create('senchaApp.view.ContactsTab'));
        // this.add(Ext.create('senchaApp.view.WeatherTab'));
        // this.add(Ext.create('senchaApp.view.GalleryTab'));
        // this.add(Ext.create('senchaApp.view.MultimediaTab'));
        // this.add(Ext.create('senchaApp.view.MapTab'));
        this.setActiveItem(0);
    }
});