Ext.define("senchaApp.view.Contacts", {
    extend: "Ext.Container",
    xtype: "contactsTab",

    config: {
        itemId: 'container',

        title: 'Kontakty',
        iconCls: 'user',

        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                title: 'Kontakty',
                id: 'titlebar',
                items: [{
                    cls: 'back',
                    hidden: true,
                    ui: 'back',
                    action: 'back',
                    align: 'left',
                    text: 'Wróć'
                }
                ]
            }, {
                html: '<h4 style="text-align: center;">Kontakty</h4>'
            }
        ]
    }
});