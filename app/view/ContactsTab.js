Ext.define("senchaApp.view.ContactsTab", {
    extend: 'Ext.Container',
    xtype: 'contactsTabPanel',

    config: {
        title: 'Kontakty',
        iconCls: 'user',
        layout: 'fit',
        defaults: {
            styleHtmlContent: true
        },

        items: [
            {
                id: 'contactsContainer',
                xtype: 'navigationview',
                defaultBackButtonText: 'Wróć',
                navigationBar: {
                    //hidden: true,
                    docked: 'top',
                    items: [
                        {
                            hidden: true,
                            xtype: 'button',
                            iconCls: 'add',
                            align: 'right',
                            id: 'addBtn'
                        },
                        {
                            hidden: true,
                            xtype: 'button',
                            //iconCls: 'trash',
                            align: 'right',
                            id: 'removeBtn',
                            text: 'Usuń'
                        },
                        {
                            hidden: true,
                            xtype: 'button',
                            //iconCls: 'action',
                            align: 'right',
                            id: 'editBtn',
                            text: 'Edytuj'
                        }]
                },
                items: [
                    {
                        xtype: 'contactsList'
                    }]
            }]
    }
});

//Ext.define("senchaApp.view.Contacts", {
//    extend: "Ext.Container",
//    xtype: "contactsTab",
//
//    config: {
//        itemId: 'container',
//
//        title: 'Kontakty',
//        iconCls: 'user',
//
//        items: [
//            {
//                docked: 'top',
//                xtype: 'titlebar',
//                title: 'Kontakty',
//                id: 'titlebar',
//                items: [{
//                    cls: 'back',
//                    hidden: true,
//                    ui: 'back',
//                    action: 'back',
//                    align: 'left',
//                    text: 'Wróć'
//                }
//                ]
//            }, {
//                html: '<h4 style="text-align: center;">Kontakty</h4>'
//            }
//        ]
//    }
//});