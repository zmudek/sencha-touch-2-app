Ext.define("senchaApp.view.multimedia.Video", {
    extend: "Ext.Container",
    xtype: "multimediaVideo",

    requires: [
        'Ext.Video'
    ],
    config: {
        layout: 'vbox',
        margin: 20,
        items: [
        {
            html: '<h4 style="text-align: center">Włodek Markowicz - Kropki</h4>'
        },
        {
            xtype: 'video',
            loop: false,
            enableControls: true,
            autoResume: true,
            url: 'http://www.bartlomiej-zmudzinski.pl/magisterka/multimedia/kropki.mp4',
            posterUrl: 'http://www.bartlomiej-zmudzinski.pl/magisterka/multimedia/kropki.jpg'
        }]
    }
});