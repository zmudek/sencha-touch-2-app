Ext.define("senchaApp.view.multimedia.Menu", {
    extend: "Ext.Container",
    xtype: "multimediaMenu",

    config: {
        title: 'Multimedia',
        defaults: {
            margin: 20
        },


        items: [
            {
                html: '<h4 style="text-align: center;">Co chcesz odtworzyć?</h4>'
            },
            {
                xtype: 'button',
                ui: 'confirm',
                text: 'Audio',
                id: 'audioBtn'
            },
            {
                xtype: 'button',
                ui: 'confirm',
                text: 'Video',
                id: 'videoBtn'
            }
        ]
    }
});