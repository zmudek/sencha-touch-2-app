Ext.define("senchaApp.view.multimedia.Audio", {
    extend: "Ext.Container",
    xtype: "multimediaAudio",

    requires: [
        'Ext.Audio'
    ],
    config: {
        margin: 20,
        items: [
        {
            html: '<h4 style="text-align: center">Scott Buckley - Wonderful</h4>'
        },
        {
            xtype: 'audio',
            enableControls: true,
            loop: false,
            url: 'http://www.bartlomiej-zmudzinski.pl/magisterka/multimedia/Scott%20Buckley%20-%20Wonderful.mp3'
        },
        {
            html: '<br><h4 style="text-align: center">Scott Buckley - Rainbiows</h4>'
        },
        {
            xtype: 'audio',
            enableControls: true,
            loop: false,
            url: 'http://www.bartlomiej-zmudzinski.pl/magisterka/multimedia/Scott%20Buckley%20-%20Rainbows.mp3'
        }]
    }
});